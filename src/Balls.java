import java.util.*;

public class Balls {

    enum Color {green, red};

    public static void main (String[] param) {
        // for debugging
        // Color[] balls = new Color[]{Color.green, Color.red, Color.green, Color.red};
        // reorder(balls);
         // for (int i = 0; i < balls.length; i++){
            // System.out.println(balls[i]);
        }
    // }

    public static void reorder (Color[] balls) {
        int redBallz = 0;
        int length = balls.length;
        for (int i = 0; i < length; i++) {
            if (balls[i] == Color.red) redBallz++;
        }
        if (redBallz == 0 || redBallz == length) return;
        for (int i = 0; i < redBallz; i++) balls[i] = Color.red;
        for (int i = redBallz; i < length; i++) balls[i] = Color.green;
    }
}